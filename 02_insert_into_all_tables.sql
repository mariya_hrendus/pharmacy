use triger_fk_cursor
go
insert into employee (  
    surname,            
    name,              
    midle_name,         
    identity_number,    
    passport,          
    experience,         
    birthday,          
    post,              
    pharmacy_id)
	values ('Hrendus', 'Mariya', 'Hryhorivna', '28765437', 'KN334521', 6, '1989-01-06', 'manager', 2),        
    ('Hirnyak', 'Andriy', 'Myronovych', '28765436', 'KB235522', 8, '1980-06-26', 'bookkeeper', 1),
	('Kryvyy', 'Serhiy', 'Ivanovych', '29765437', 'KL334521', 10, '1978-11-25', 'pharmacist', 3),
	('Myronyuk', 'Ilona', 'Ihorivna', '29565437', 'KS334521', 3, '1991-02-13', 'office manager', 2),
	('Helysh', 'Nadiya', 'Olehivna', '28765255', 'KV334588', 13, '1975-04-22', 'administrator', 3),
	('Matviy', 'Solomiya', 'Hryhorivna', '28222437', 'KN675521', 6, '1990-10-21', 'cleaner', 4),
	('Vyklyuk', 'Iryna', 'Ivanivna', '56465437', 'KN334999', 5, '1989-12-06', 'manager', 5),
	('Mykyta', 'Halyna', 'Hryhorivna', '33365437', 'KL334598', 2, '1980-05-11', 'administrator', 4),
	('Chorna', 'Olha', 'Mykhailivna', '23665437', 'KB774521', 8, '1981-07-16', 'manager', 5),
	('Barnas', 'Liliya', 'Yaroslavivna', '22765437', 'KZ334521', 5, '1983-08-26', 'manager', 1)
	go 
	insert into medicine (
	name,              
    ministry_code)
	values ('Mukaltyn', 'Code A'),
	('Strepsils', 'Code B'),
	('Folio', 'Code A'),
	('Khofitol', 'Code C'),
	('Streptotsyd', 'Code D'),
	('Mezym', 'Code C'),
	('Visine', 'Code D'),
	('Menovazyn', 'Code B'),
	('Aspiryn', 'Code C'),
	('Yodomaryn', 'Code B')
	go
	insert into medicine_zone (
    medicine_id, zone_id)
	values (1,3), (2,8), (3,6), (4,5), (9,2), (1,7), (10,3), (2,7), (3,3), (10,5), (7,3), (3,4), (8,1), (9,10)
	go
	insert into pharmacy (
	name,               
    building_number,    
    www,                
    street             
    )
	values ('3i', '12', '3i.org.ua', 'Shevchenka'),
	('Podorozhnyk', '25', 'podorozhnyk.com.ua', 'Aviatsiyna'),
	('BAM', '11', 'asbam.com', 'Hlyboka'),
	('Znakhar', '29', 'znakhar.ua', 'Bandery'),
	('Zdorovya', '75', 'zdorovya.org.ua', 'Zelena'),
	('Biomed', '128', 'biomed.org.ua', 'Sonyachna'),
	('DS', '355', 'ds.org.ua', 'Horodotska'),
	('Watsons', '98', 'watsons.com.ua', 'Patona'),
	('Apteka', '187', 'apteka.ua', 'Vyhovskoho'),
	('Optymalna', '56', 'optymalna.org.ua', 'Okruzhna')
	go
	insert into pharmacy_medicine (
	pharmacy_id,    
    medicine_id)
	values (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),
    (2,1),(2,2),(2,3),(2,4),(2,5),(2,6),(2,7),(2,8),(2,9),(2,10),
	(3,1),(3,2),(3,3),(3,4),(3,5),(3,6),(3,7),(3,8),(3,9),(3,10),
	(4,1),(4,2),(4,3),(4,4),(4,5),(4,6),(4,7),(4,8),(4,9),(4,10),
	(5,1),(5,2),(5,3),(5,4),(5,5),(5,6),(5,7),(5,8),(5,9),(5,10),
	(6,1),(6,2),(6,3),(6,4),(6,5),(6,6),(6,7),(6,8),(6,9),(6,10),
	(7,1),(7,2),(7,3),(7,4),(7,5),(7,6),(7,7),(7,8),(7,9),(7,10),
	(8,1),(8,2),(8,3),(8,4),(8,5),(8,6),(8,7),(8,8),(8,9),(8,10),
	(9,1),(9,2),(9,3),(9,4),(9,5),(9,6),(9,7),(9,8),(9,9),(9,10),
	(10,1),(10,2),(10,3),(10,4),(10,5),(10,6),(10,7),(10,8),(10,9),(10,10)
	go
	insert into post (
	post)
	values ('manager'),('bookkeeper'),('administrator'), ('cleaner'), ('pharmacist'), ('office manager')
	go
	insert into street (street)
	values ('Shevchenka'), ('Aviatsiyna'), ('Hlyboka'), ('Bandery'), ('Zelena'), ('Sonyachna'),
	('Horodotska'),('Patona'),('Vyhovskoho'), ('Okruzhna')
	go
	insert into zone (name)
	values ('head'), ('heart'),('eyes'), ('ears'), ('nose'), ('kidneys'), ('stomach'), ('throat'), ('milt'), ('teeth')
	go
	  


